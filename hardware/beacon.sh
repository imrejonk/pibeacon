# Bring Bluetooth chip online
hciconfig hci0 up

# Set BLE mode
hciconfig hci0 leadv 3

# Disable scanning as it is not needed
hciconfig hci0 noscan

# Start beacon with UUID 80dfe4f5-07c0-4093-ae4d-cebc0a10f391, major 200 and minor 60
#                              |         iBeacon prefix      |                   UUID                        |Major|Minor|TXPWR
hcitool -i hci0 cmd 0x08 0x0008 1E 02 01 1A 1A FF 4C 00 02 15 80 DF E4 F5 07 C0 40 93 AE 4D CE BC 0A 10 F3 91 00 C8 00 3C C8 00
