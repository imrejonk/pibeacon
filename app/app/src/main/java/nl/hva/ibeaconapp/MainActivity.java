package nl.hva.ibeaconapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "MainActivity";
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private Handler scanHandler = new Handler();
    private int scan_interval_ms = 5000;
    private boolean isScanning = false;

    /**
     * onCreate() method
     *
     * Instructs the Bluetooth manager to provide us with
     * a BLE adapter which we then use to detect iBeacons.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize BLE adapter
        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        // Launch scan threat
        scanHandler.post(scanRunnable);
    }

    private Runnable scanRunnable = new Runnable()
    {

        /**
         * run() method
         *
         * Creates the BLE scan background threat.
         */
        @Override
        public void run() {

            if (isScanning)
            {
                if (btAdapter != null)
                {
                    // Stop scan callback
                    btAdapter.stopLeScan(leScanCallback);
                }
            }
            else
            {
                if (btAdapter != null)
                {
                    // Start scan callback
                    btAdapter.startLeScan(leScanCallback);
                }
            }

            isScanning = !isScanning;

            scanHandler.postDelayed(this, scan_interval_ms);
        }
    };

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback()
    {

        /**
         * onLeScan() method
         *
         * Extracts the UUID, Major and Minor from the scans.
         */
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord)
        {
            int startByte = 2;
            boolean patternFound = false;
            while (startByte <= 5)
            {
                if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && // This is an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) // Correct data length
                {
                    patternFound = true;
                    break;
                }

                startByte++;
            }

            if (patternFound)
            {
                // Convert to hex String
                byte[] uuidBytes = new byte[16];
                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                String hexString = bytesToHex(uuidBytes);

                // Detect UUID
                String uuid =  hexString.substring(0,8) + "-" +
                        hexString.substring(8,12) + "-" +
                        hexString.substring(12,16) + "-" +
                        hexString.substring(16,20) + "-" +
                        hexString.substring(20,32);

                final int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);
                final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);

                TextView textUUID = findViewById(R.id.uuidResult);
                textUUID.setText(uuid);

                TextView textMajor = findViewById(R.id.majorResult);
                textMajor.setText(Integer.toString(major));

                TextView textMinor = findViewById(R.id.minorResult);
                textMinor.setText(Integer.toString(minor));

                Log.i(LOG_TAG,"UUID: " +uuid + "\\nmajor: " +major +"\\nminor" +minor);
            }

        }
    };

    /**
     * bytesToHex() method
     *
     * Converts bytes to hexadecimal characters.
     */
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
