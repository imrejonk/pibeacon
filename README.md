# PiBeacon

Raspberry Pi iBeacon and accompanying Android app for the AUAS HBO-ICT thematic semester IoT, course Platform, Sensors and Communication.  
Authors: Atif Omerovic, Imre Jonk  
Home page: https://www.imrejonk.nl/pibeacon/

## Hardware

A Raspberry Pi 3 can be used as an iBeacon by configuring it with hardware/beacon.sh. The script contains our pre-programmed UUID 80dfe4f5-07c0-4093-ae4d-cebc0a10f391, but this can be changed easily. It runs the [BlueZ](http://www.bluez.org/) tools to configure the Bluetooth chip. The file hardware/rc.local contains a sample /etc/rc.local script that can be used to start the iBeacon on boot.

## Android app

We wrote an Android application that detects the iBeacon and shows the UUID, Major and Minor. For the assignment the Major was chosen to be 200 and the Minor 60. The app needs these four Android permissions to function:

- android.permission.BLUETOOTH; 
- android.permission.BLUETOOTH\_ADMIN;
- android.permission.ACCESS\_COARSE\_LOCATION;
- android.permission.ACCESS\_FINE\_LOCATION.

We further used the library org.altbeacon:android-beacon-library, which contains APIs for using Android beacon functionality. By using this library instead of reinventing the wheel we adhere to the UNIX philosophy, saying that you should do one thing and do it well.
